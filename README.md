Brain Time vs Body Time
=======================
Inspired by the chapter "Brain time versus Body Time" in the book "Peopleware -
Productive projects and teams" by Tom DeMarco and Timothy Lister this android
application enables you to log your time and interrupts. From the logged
timestamps it is then possible to calculate the amount of uninterrupted hours
and in the end your Environmental Factor.

Book is available from Informit [Peopleware - Productive projects and
teams (third edition)](http://www.informit.com/store/peopleware-productive-projects-and-teams-9780321934116)

### Main functionality
 * Log time and interrupts

### Extra functionality
 * Export logged time and interrupts
 * Calculate Environmental Factor (E-Factor = Uninterrupted Hours divided by
   Body-Present Hours) (Not yet implemented)
 * Filter specific activities to always be "body time", like meetings. (Not yet
   implemented)
