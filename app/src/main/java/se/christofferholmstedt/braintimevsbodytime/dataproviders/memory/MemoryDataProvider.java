package se.christofferholmstedt.braintimevsbodytime.dataproviders.memory;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.common.DataProvider;

public class MemoryDataProvider
        implements DataProvider {

    private Integer idCounter = 0;
    private List<Activity> allActivities = new ArrayList<>();

    @Override
    public boolean add(Activity activity) {
        Optional<Activity> tmpActivity;

        if (activity.getStopTime().isPresent())
        {
            tmpActivity = Optional.of(new Activity(idCounter,
                    activity.getStartTime(),
                    activity.getStopTime().get(),
                    activity.getInterrupts(),
                    activity.getTags())
            );
        } else {
            tmpActivity = Optional.of(new Activity(idCounter,
                    activity.getStartTime(),
                    activity.getInterrupts(),
                    activity.getTags())
            );
        }

        this.idCounter++;
        this.allActivities.add(tmpActivity.get());

        return true;
    }

    @Override
    public Optional<Activity> read(Integer id) {
        Log.e("MemoryDataProvider", "Reading one activity");
        return this.allActivities.stream().filter(hasId(id)).findFirst();
    }

    @Override
    public List<Activity> readAll(){
        Log.e("MemoryDataProvider", "Reading all activities");
        return this.allActivities;
    }

    @Override
    public Optional<Activity> update(Integer id, Activity activity) {
        Log.e("MemoryDataProvider", "Update " + id);
        Optional<Activity> activityWithId = this.allActivities.stream().filter(hasId(id)).findFirst();

        if (activityWithId.isPresent())
        {
            Log.e("MemoryDataProvider", "Remove one and add one Activity");
            this.allActivities.remove(activityWithId.get());
            this.allActivities.add(activity);
            return Optional.of(activity);
        }
        else
        {
            return Optional.empty();
        }
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    private static Predicate<Activity> hasId(Integer id) {
        return a -> a.getId().isPresent() && a.getId().get().equals(id);
    }
}