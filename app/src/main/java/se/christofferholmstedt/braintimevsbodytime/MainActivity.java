package se.christofferholmstedt.braintimevsbodytime;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Boolean currentlyLogging = false;
    private ToggleButton timerButton;
    private Button interruptButton;
    private AutoCompleteTextView activityName;
    private Date startTime;
    private Date stopTime;
    private DateFormat iso8601DateTimeFormat;
    private DateFormat tableDatePresentationFormat;
    private LinkedList<String> usedActivityNames;
    private DatabaseHelper dbHelper;
    private long lastActivityId;
    private TableLayout loggedActivities;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loggedActivities = (TableLayout) findViewById(R.id.loggedActivities);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, usedActivityNames);
        timerButton = (ToggleButton) findViewById(R.id.toggleButton);
        activityName = (AutoCompleteTextView) findViewById(R.id.editText);
        activityName.setAdapter(adapter);
        interruptButton = (Button) findViewById(R.id.interruptButton);
        interruptButton.setEnabled(false);
        iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        tableDatePresentationFormat = new SimpleDateFormat("HH:mm");

        dbHelper = new DatabaseHelper(this);
        usedActivityNames = getLoggedActivityNamesAsList();
        redrawLoggedActivites();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("activity", String.valueOf(activityName.getText()));
        savedInstanceState.putLong("currentActivityDatabaseID", lastActivityId);
        savedInstanceState.putBoolean("currentlyLogging", currentlyLogging);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        activityName.setText(savedInstanceState.getString("activity"));
        lastActivityId = savedInstanceState.getLong("currentActivityDatabaseID");
        currentlyLogging = savedInstanceState.getBoolean("currentlyLogging");

        if (currentlyLogging)
        {
            interruptButton.setEnabled(true);
            activityName.setEnabled(false);
        } else {
            interruptButton.setEnabled(false);
            activityName.setEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.extra_features, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send_email:
                sendEmail();
                return true;
            case R.id.delete_data:
                AlertDialog deleteDialog = askOption();
                deleteDialog.show();
                return true;
            case R.id.about:
                AlertDialog aboutDialog = aboutDialog();
                aboutDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void toggleTimer(View view) {

        if (timerButton.isChecked())
        {
            currentlyLogging = true;
            String currentActivity = activityName.getText().toString().trim();
            activityName.setText(currentActivity);
            activityName.setEnabled(false);
            activityName.dismissDropDown();
            interruptButton.setEnabled(true);

            if (!usedActivityNames.contains(currentActivity) && !currentActivity.equals(""))
            {
                usedActivityNames.add(currentActivity);
                updateUsedActivitiesNamesAdapter();
            }

            if (currentActivity.equals(""))
            {
                currentActivity = "Default project";
            }

            startTime = new Date();
            String timeString = iso8601DateTimeFormat.format(startTime);
            Toast.makeText(MainActivity.this, "Activity started at " + timeString, Toast.LENGTH_LONG).show();
            this.lastActivityId = dbHelper.setStartTime(currentActivity, timeString, "Not set");
        }
        else
        {
            stopTime = new Date();
            String timeString = iso8601DateTimeFormat.format(stopTime);
            Toast.makeText(MainActivity.this, "Activity stopped at " + timeString, Toast.LENGTH_LONG).show();

            activityName.setEnabled(true);
            interruptButton.setEnabled(false);
            currentlyLogging = false;
            dbHelper.setStopTime(this.lastActivityId, timeString);
        }

        redrawLoggedActivites();
    }

    private void updateUsedActivitiesNamesAdapter() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, usedActivityNames);
        activityName.setAdapter(adapter);
    }

    private void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Brain Time vs Body Time (CSV)");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Logged activities:\n" + getLoggedActivitiesAsCSV() + "\nInterrupts:\n" + getInterruptsAsCSV());

        try {
            startActivity(Intent.createChooser(emailIntent, "Send email using..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "No email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteActivitiesAndInterrupts() {
        this.dbHelper.dropTables();
        timerButton.setChecked(false);
        activityName.setEnabled(true);
    }

    private AlertDialog askOption()
    {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Do you want to delete all logged data?")
                .setIcon(android.R.drawable.ic_delete)

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteActivitiesAndInterrupts();
                        redrawLoggedActivites();
                        dialog.dismiss();
                    }

                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    public void onInterruptClick(View view)
    {
        startTime = new Date();
        String timeString = iso8601DateTimeFormat.format(startTime);
        Toast.makeText(MainActivity.this, "Interrupt logged at " + timeString, Toast.LENGTH_LONG).show();
        dbHelper.setInterruptTime(timeString);
        redrawLoggedActivites();
    }

    private void redrawLoggedActivites()
    {
        Activity lastActivity = new Activity();
        loggedActivities.removeAllViews();
        List<Activity> tmp = this.dbHelper.getActivities();
        if (!tmp.isEmpty())
        {
            for (Activity activity : tmp)
            {
                TableRow row = new TableRow(this);
                TableLayout.LayoutParams lp =
                        new TableLayout.LayoutParams
                                (TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(2, 2, 2, 2);
                row.setLayoutParams(lp);

                TextView activityNameCell = new TextView(this);
                activityNameCell.setText(activity.getName());

                TextView activityStartTime = new TextView(this);
                activityStartTime.setText(tableDatePresentationFormat.format(activity.getStartTimeAsDate()));
                activityStartTime.setPadding(10,0,0,0);

                TextView activityStopTime = new TextView(this);

                if (activity.getStopTimeAsDate() != null) {
                    activityStopTime.setText(tableDatePresentationFormat.format(activity.getStopTimeAsDate()));
                } else {
                    activityStopTime.setText(activity.getStopTime());
                }

                activityStopTime.setPadding(10,0,0,0);

                TextView activityInterrupts = new TextView(this);
                activityInterrupts.setText(Integer.toString(getUninterruptedHours(activity)));
                activityInterrupts.setPadding(10,0,0,0);

                row.addView(activityNameCell);
                row.addView(activityStartTime);
                row.addView(activityStopTime);
                row.addView(activityInterrupts);

                loggedActivities.addView(row, lp);
                lastActivity = activity;
            }

            if (lastActivity.getId() != -1 && lastActivity.getStopTime().equals("Not set"))
            {
                currentlyLogging = true;
                interruptButton.setEnabled(true);
                activityName.setEnabled(false);
                timerButton.setChecked(true);
                lastActivityId = lastActivity.getId();
            }
        }
    }

    private String getLoggedActivitiesAsCSV()
    {
        String result = new String();

        List<Activity> listOfActivities = this.dbHelper.getActivities();
        for (Activity activity : listOfActivities) {
            result = result + activity.getName() + "," + activity.getStartTime() + "," + activity.getStopTime() + "\n";
        }

        return result;
    }

    private String getInterruptsAsCSV()
    {
        String result = new String();

        List<String> listOfInterrupts = this.dbHelper.getInterrupts();
        for (String string: listOfInterrupts) {
            result = result + string + "\n";
        }

        return result;
    }

    private LinkedList<String> getLoggedActivityNamesAsList() {
        LinkedList<String> result = new LinkedList<>();

        List<Activity> listOfActivities = this.dbHelper.getActivities();
        for (Activity activity : listOfActivities)
        {
            if (!result.contains(activity.getName()))
            {
                result.add(activity.getName());
            }
        }

        return result;
    }

    private int getUninterruptedHours(Activity activity) {
        return getUninterruptedHours(activity.getStartTimeAsDate(),
                                        activity.getStopTimeAsDate(),
                                        activity.getInterrupts());
    }

    public static int getUninterruptedHours(Date start, Date stop, List<Date> interrupts) {
        long hour = 3600*1000;
        long twentyMinutes = 20*60*1000;

        if (stop == null || start == null)
        {
            return 0;
        }

        Date startPlusOneHour = new Date(start.getTime() + 1 * hour);
        Date startMinusTwentyMinutes = new Date(start.getTime() - twentyMinutes);

        if (startPlusOneHour.after(stop))
        {
            return 0;
        }
        else if (interrupts.size() == 0)
        {
            return getUninterruptedHours(startPlusOneHour, stop, interrupts) + 1;
        }
        else
        {
            Date interrupt = interrupts.get(0);
            List<Date> newList = new ArrayList<>(interrupts);
            newList.remove(0);
            if (interrupt.after(startMinusTwentyMinutes) && interrupt.before(startPlusOneHour))
            {
                return getUninterruptedHours(
                        new Date(interrupt.getTime() + twentyMinutes),
                                    stop, newList);
            }
            else
            {
                return getUninterruptedHours(startPlusOneHour, stop, interrupts) + 1;
            }
        }
    }

    private AlertDialog aboutDialog()
    {
        String aboutText =
                "Inspired by the chapter \"Brain time versus Body Time\" " +
                        "in the book \"Peopleware - Productive projects and " +
                        "teams\" by Tom DeMarco and Timothy Lister this android " +
                        "application enables you to log your time and interrupts. " +
                        "From the logged timestamps it is then possible to " +
                        "calculate the amount of uninterrupted hours and in " +
                        "the end your Environmental Factor.\n" +
                "\n" +
                "Source code and issue tracking available on Github at " +
                "https://github.com/christofferholmstedt/braintimevsbodytime\n" +
                "\n" +
                "MIT License\n" +
                "\n" +
                "Copyright (c) 2018 Christoffer Holmstedt <christoffer.holmstedt@gmail.com>\n" +
                "\n" +
                "Permission is hereby granted, free of charge, to any person obtaining a copy " +
                "of this software and associated documentation files (the \"Software\"), to deal " +
                "in the Software without restriction, including without limitation the rights " +
                "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell " +
                "copies of the Software, and to permit persons to whom the Software is " +
                "furnished to do so, subject to the following conditions:\n" +
                "\n" +
                "The above copyright notice and this permission notice shall be included in all " +
                "copies or substantial portions of the Software.\n" +
                "\n" +
                "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR " +
                "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, " +
                "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE " +
                "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER " +
                "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, " +
                "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE " +
                "SOFTWARE.";

        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("About")
                .setCancelable(false)
                .setMessage(aboutText)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }

                })
                .create();
        return myQuittingDialogBox;
    }
}
