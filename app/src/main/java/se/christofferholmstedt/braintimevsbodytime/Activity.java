package se.christofferholmstedt.braintimevsbodytime;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Activity {
    private long id;
    private String name;
    private String startTime;
    private String stopTime;
    private Date startTimeAsDate;
    private Date stopTimeAsDate;
    private List<Date> interrupts;
    private DateFormat iso8601DateTimeFormat;

    public Activity () {
        id = -1;
        name = "Default project";
        startTime = "Not set";
        stopTime = "Not set";
        interrupts = new ArrayList<>();
        iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;

        try {
            this.startTimeAsDate = iso8601DateTimeFormat.parse(startTime);
        } catch (ParseException e) {
            Log.w("parseDate", "Parsing start time string to date failed.");
        }
    }

    public String getStopTime() {
        return stopTime;
    }

    public Date getStartTimeAsDate() {
        return startTimeAsDate;
    }

    public void setStartTimeAsDate(Date startTimeAsDate) {
        this.startTimeAsDate = startTimeAsDate;
        this.startTime = iso8601DateTimeFormat.format(startTimeAsDate);
    }

    public Date getStopTimeAsDate() {
        return stopTimeAsDate;
    }

    public void setStopTimeAsDate(Date stopTimeAsDate) {
        this.stopTimeAsDate = stopTimeAsDate;
        this.stopTime = iso8601DateTimeFormat.format(stopTimeAsDate);
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;

        try {
            this.stopTimeAsDate = iso8601DateTimeFormat.parse(stopTime);
        } catch (ParseException e) {
            Log.w("parseDate", "Parsing stop time string to date failed.");
        }
    }

    public void addInterrupt(Date interrupt) {
        this.interrupts.add(interrupt);
    }

    public List<Date> getInterrupts () {
        return this.interrupts;
    }
}
