package se.christofferholmstedt.braintimevsbodytime.entrypoints.gui.simple;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ViewModel {

    private List<ViewModelItem> allItems = new ArrayList<>();
    private Optional<ViewModelItem> activeItem = Optional.empty();

    protected ViewModel() {}

    public List<ViewModelItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<ViewModelItem> allItems) {
        this.allItems = allItems;
    }

    public Optional<ViewModelItem> getActiveItem() {
        return activeItem;
    }

    public void setActiveItem(Optional<ViewModelItem> activeItem) {
        this.activeItem = activeItem;
    }


}
