package se.christofferholmstedt.braintimevsbodytime.entrypoints.gui.simple;

public interface PresenterListener {
    void updateView(ViewModel viewModel);
}
