package se.christofferholmstedt.braintimevsbodytime.entrypoints.gui.simple;

import android.util.Log;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseModel;
import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

public class Presenter implements ActivitiesResponseBoundary {

    private List<PresenterListener> listeners = new ArrayList<>();
    private ViewModel viewModel = new ViewModel();

    public Presenter () {}

    public void addListener(PresenterListener listener)
    {
        if (!this.listeners.contains(listener))
        {
            this.listeners.add(listener);
            listener.updateView(this.viewModel);
        }
        Log.e("Presenter", "Listeners: " + this.listeners.size());
    }

    public void removeListener(PresenterListener listener)
    {
        if (this.listeners.contains(listener))
        {
            this.listeners.remove(listener);
        }
    }

    private void onViewModelChange()
    {
        listeners.forEach( (listener) -> listener.updateView(this.viewModel) );
    }

    @Override
    public void updatePresentationLayer(ActivitiesResponseModel activitiesResponseModel) {
        List<ViewModelItem> tempList = new ArrayList<>();

        activitiesResponseModel.getActivities().forEach(n -> tempList.add(activityToViewModelItem(n)));
        this.viewModel.setAllItems(tempList);

        this.viewModel.setActiveItem(this.viewModel.getAllItems().stream().filter(n -> n.getActiveItem()).findFirst());

        onViewModelChange();
    }

    private ViewModelItem activityToViewModelItem(Activity activity)
    {
        ViewModelItem result = new ViewModelItem();

        result.setId(activity.getId().isPresent() ? activity.getId().get().toString() : "");
        result.setStartTime(activity.getStartTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        result.setStopTime(activity.getStopTime().isPresent() ? activity.getStopTime().get().format(DateTimeFormatter.ofPattern("HH:mm")) : "");
        result.setInterrupts(activity.getInterrupts().stream().map(zonedDateTimeStringFunction).collect(Collectors.toList()));
        result.setTags(activity.getTags());
        result.setActiveItem(!activity.getStopTime().isPresent());

        return result;
    }

    private Function<ZonedDateTime, String> zonedDateTimeStringFunction
            = new Function<ZonedDateTime, String>() {

        @Override
        public String apply(ZonedDateTime zonedDateTime) {
            return zonedDateTime.format(DateTimeFormatter.ofPattern("HH:mm"));
        }
    };
}
