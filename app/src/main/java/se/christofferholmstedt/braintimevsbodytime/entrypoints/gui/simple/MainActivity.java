package se.christofferholmstedt.braintimevsbodytime.entrypoints.gui.simple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import se.christofferholmstedt.braintimevsbodytime.R;
import se.christofferholmstedt.braintimevsbodytime.configurations.ApplicationConfiguration;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.AddTagBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.InterruptActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.StartActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.StopActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.ViewActivitiesBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.addtag.TagAlreadyAddedException;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.startactivity.ActivityAlreadyStartedException;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.stopactivity.ActivityNotStartedException;

public class MainActivity extends AppCompatActivity implements PresenterListener {

    private ToggleButton timerButton;
    private Button interruptButton;
    private Button addTagButton;
    private AutoCompleteTextView tagText;
    private TableLayout loggedActivities;
    private StartActivityBoundary startActivity;
    private StopActivityBoundary stopActivity;
    private InterruptActivityBoundary interruptActivity;
    private AddTagBoundary addTag;
    private Presenter presenter;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_activity_main);
        timerButton = findViewById(R.id.simple_toggle_activity);
        interruptButton = findViewById(R.id.simple_interrupt_button);
        addTagButton = findViewById(R.id.simple_add_tag_button);
        tagText = findViewById(R.id.simple_tag_text_view);
        loggedActivities = findViewById(R.id.simple_logged_activities);
        viewModel = new ViewModel();

        ApplicationConfiguration configuration = ApplicationConfiguration.getInstance();
        startActivity = configuration.getStartActivityBoundary();
        stopActivity = configuration.getStopActivityBoundary();
        interruptActivity = configuration.getInterruptActivityBoundary();
        addTag = configuration.getAddTagBoundary();
        ViewActivitiesBoundary viewActivities = configuration.getViewActivitiesBoundary();
        viewActivities.getAllActivities();
        presenter = configuration.getPresenter();
        presenter.addListener(this);

    }

    public void toggleActivityTimer(View view) {
        if (timerButton.isChecked()) {
            try
            {
                startActivity.start();
            } catch (ActivityAlreadyStartedException e) {
                Toast.makeText(MainActivity.this, "Activity already started, cannot start another.", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            try
            {
                if (viewModel.getActiveItem().isPresent())
                {
                    this.stopActivity.stopActivity(Integer.valueOf(viewModel.getActiveItem().get().getId()));
                }
            } catch (ActivityNotStartedException e)
            {
                Toast.makeText(MainActivity.this, "Activity has not been started, cannot stop any activity.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void interrupt(View view)
    {
        try
        {
            if (viewModel.getActiveItem().isPresent())
            {
                this.interruptActivity.interruptActivity(Integer.valueOf(viewModel.getActiveItem().get().getId()));
            }
        } catch (ActivityNotStartedException e)
        {
            Toast.makeText(MainActivity.this, "Activity has not been started, cannot interrupt any activity.", Toast.LENGTH_SHORT).show();
        }
    }

    public void addTag(View view)
    {
        try
        {
            if (viewModel.getActiveItem().isPresent())
            {
                this.addTag.addTag(Integer.valueOf(viewModel.getActiveItem().get().getId()), tagText.getText().toString().trim()); ;
            }
        }
        catch (ActivityNotStartedException e)
        {
            Toast.makeText(MainActivity.this, "Activity has not been started, cannot add tag to any activity.", Toast.LENGTH_SHORT).show();
        }
        catch (TagAlreadyAddedException e)
        {
            Toast.makeText(MainActivity.this, "Tag \"" + tagText.getText().toString().trim() + "\" has already been added to this activity", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void updateView(ViewModel viewModel)
    {
        //TODO: Update view to ListView which uses the "Adapter" to populate data instead of memory inefficient TableLayout
        this.viewModel = viewModel;
        updateButtons();
        loggedActivities.removeAllViews();
        for (ViewModelItem viewModelItem : viewModel.getAllItems())
        {
            TableRow row = new TableRow(this);
            TableLayout.LayoutParams lp =
                    new TableLayout.LayoutParams
                            (TableLayout.LayoutParams.MATCH_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(2, 2, 2, 2);
            row.setLayoutParams(lp);

            TextView activityId = new TextView(this);
            activityId.setText(viewModelItem.getId());
            activityId.setPadding(10,0,0,0);

            TextView activityActive = new TextView(this);
            activityActive.setText(viewModelItem.getActiveItem().toString());
            activityActive.setPadding(10,0,0,0);

            TextView activityStartTime = new TextView(this);
            activityStartTime.setText(viewModelItem.getStartTime());
            activityStartTime.setPadding(10,0,0,0);

            TextView activityStopTime = new TextView(this);
            activityStopTime.setText(viewModelItem.getStopTime());
            activityStopTime.setPadding(10,0,0,0);

            TextView activityInterrupts = new TextView(this);
            activityInterrupts.setText(String.join(" ", viewModelItem.getInterrupts()));
            activityInterrupts.setPadding(10,0,0,0);

            TextView activityTags = new TextView(this);
            activityTags.setText(String.join(" ", viewModelItem.getTags()));
            activityTags.setPadding(10,0,0,0);

            row.addView(activityId);
            row.addView(activityActive);
            row.addView(activityStartTime);
            row.addView(activityStopTime);
            row.addView(activityInterrupts);
            row.addView(activityTags);

            loggedActivities.addView(row, lp);
        }
    }

    private void updateButtons() {
        if (this.viewModel.getActiveItem().isPresent())
        {
            tagText.setHint("Write tag name here");
            tagText.setEnabled(true);
            addTagButton.setEnabled(true);
            interruptButton.setEnabled(true);
        }
        else
        {
            tagText.setHint("Start activity...");
            tagText.setEnabled(false);
            addTagButton.setEnabled(false);
            interruptButton.setEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.removeListener(this);
    }
}
