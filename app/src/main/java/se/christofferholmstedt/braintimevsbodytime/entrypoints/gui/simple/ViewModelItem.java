package se.christofferholmstedt.braintimevsbodytime.entrypoints.gui.simple;

import java.util.ArrayList;
import java.util.List;

public class ViewModelItem {
    private String id;
    private String startTime;
    private String stopTime;
    private List<String> interrupts;
    private List<String> tags;
    private Boolean isActiveItem;

    public ViewModelItem()
    {
        this.id = "";
        this.startTime = "";
        this.stopTime = "";
        this.interrupts = new ArrayList<>();
        this.tags = new ArrayList<>();
        this.isActiveItem = false;

    }

    public ViewModelItem(String id, String startTime, String stopTime, List<String> interrupts, List<String> tags) {
        this.id = id;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.interrupts = interrupts;
        this.tags = tags;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setInterrupts(List<String> interrupts) {
        this.interrupts = interrupts;
    }

    public List<String> getInterrupts() {
        return interrupts;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getTags() {
        return tags;
    }

    public Boolean getActiveItem() {
        return isActiveItem;
    }

    public void setActiveItem(Boolean activeItem) {
        isActiveItem = activeItem;
    }
}
