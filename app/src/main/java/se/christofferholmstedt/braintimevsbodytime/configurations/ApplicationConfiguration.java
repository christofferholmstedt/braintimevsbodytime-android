package se.christofferholmstedt.braintimevsbodytime.configurations;

import java.util.Optional;

import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.AddTagBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.InterruptActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.StartActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.StopActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.ViewActivitiesBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.addtag.AddTag;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.interruptactivity.InterruptActivity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.startactivity.StartActivity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.stopactivity.StopActivity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.viewactivities.ViewActivities;
import se.christofferholmstedt.braintimevsbodytime.dataproviders.memory.MemoryDataProvider;
import se.christofferholmstedt.braintimevsbodytime.entrypoints.gui.simple.Presenter;

public final class ApplicationConfiguration {

    private static Optional<ApplicationConfiguration> instance = Optional.empty();

    private StartActivityBoundary startActivityBoundary;
    private StopActivityBoundary stopActivityBoundary;
    private ViewActivitiesBoundary viewActivitiesBoundary;
    private InterruptActivityBoundary interruptActivityBoundary;
    private AddTagBoundary addTagBoundary;
    private MemoryDataProvider dataProvider;
    private Presenter presenter;

    private ApplicationConfiguration ()
    {
        // GUI
        this.presenter = new Presenter();

        // Business logic and data providers
        this.dataProvider = new MemoryDataProvider();
        this.startActivityBoundary = new StartActivity(this.presenter, this.dataProvider);
        this.stopActivityBoundary = new StopActivity(this.presenter, this.dataProvider);
        this.viewActivitiesBoundary = new ViewActivities(this.presenter, this.dataProvider);
        this.interruptActivityBoundary = new InterruptActivity(this.presenter, this.dataProvider);
        this.addTagBoundary = new AddTag(this.presenter, this.dataProvider);
    };

    public static ApplicationConfiguration getInstance()
    {
        if (instance.isPresent()) return instance.get();

        instance = Optional.of(new ApplicationConfiguration());
        return instance.get();
    }

    public StartActivityBoundary getStartActivityBoundary()
    {
        return this.startActivityBoundary;
    }

    public StopActivityBoundary getStopActivityBoundary() {
        return stopActivityBoundary;
    }

    public ViewActivitiesBoundary getViewActivitiesBoundary() { return this.viewActivitiesBoundary; }

    public Presenter getPresenter() { return this.presenter; }

    public InterruptActivityBoundary getInterruptActivityBoundary() {
        return interruptActivityBoundary;
    }

    public AddTagBoundary getAddTagBoundary() {
        return addTagBoundary;
    }
}
