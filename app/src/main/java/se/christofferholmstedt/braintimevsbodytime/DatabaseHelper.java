package se.christofferholmstedt.braintimevsbodytime;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    static final String databaseName="braintimevsbodytime";

    static final String tableNameLoggedActivities="logged_activities";
    static final String colID="id";
    static final String colActivity="activity";
    static final String colStartTime="start_time";
    static final String colStopTime="stop_time";

    static final String tableNameInterrupts="interrupts";
    static final String colInterruptsID="id";
    static final String colInterruptTime="interrupt_time";

    public DatabaseHelper(Context context) {
        super(context, databaseName, null,1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + tableNameLoggedActivities
                + "(" +
                colID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                colActivity + " TEXT, " +
                colStartTime + " TEXT, " +
                colStopTime + " TEXT); ");

        db.execSQL("CREATE TABLE " + tableNameInterrupts
                + "(" +
                colInterruptsID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                colInterruptTime + " TEXT); ");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + tableNameLoggedActivities);
        db.execSQL("DROP TABLE IF EXISTS " + tableNameInterrupts);
        onCreate(db);
    }

    public long setStartTime(String activity, String start_time, String stop_time)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(colActivity, activity);
        cv.put(colStartTime, start_time);
        cv.put(colStopTime, stop_time);
        long rowID = db.insert(tableNameLoggedActivities, null, cv);
        db.close();

        return rowID;
    }

    public int setStopTime(long id, String stop_time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(colStopTime, stop_time);
        Integer affectedRows = db.update(tableNameLoggedActivities, cv, colID + "=?",
                new String []{String.valueOf(id)});
        db.close();

        return affectedRows;
    }

    public long setInterruptTime(String interrupt_time)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(colInterruptTime, interrupt_time);
        long rowID = db.insert(tableNameInterrupts, null, cv);
        db.close();

        return rowID;
    }

    public List<Activity> getActivities() {
        List<Activity> result = new ArrayList<>();
        List<String> interrupts = this.getInterrupts();
        List<Date> interruptsAsDates = parseDates(interrupts);

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + tableNameLoggedActivities, new String [] {});

        while (cur.moveToNext()) {
            Activity activity = new Activity();
            activity.setId(cur.getLong(cur.getColumnIndex(colID)));
            activity.setName(cur.getString(cur.getColumnIndex(colActivity)));
            activity.setStartTime(cur.getString(cur.getColumnIndex(colStartTime)));
            activity.setStopTime(cur.getString(cur.getColumnIndex(colStopTime)));

            for (Date interrupt : interruptsAsDates)
            {
                if (isInterruptDuringActivity(activity, interrupt))
                {
                    activity.addInterrupt(interrupt);
                }
            }
            result.add(activity);
        }

        return result;
    }

    private boolean isInterruptDuringActivity(Activity activity, Date interrupt) {
        return (interrupt.after(activity.getStartTimeAsDate()) &&
                        (!activity.getStopTime().equals("Not set") &&
                        interrupt.before(activity.getStopTimeAsDate())))

                || (interrupt.after(activity.getStartTimeAsDate()) &&
                        activity.getStopTime().equals("Not set"));
    }

    private List<Date> parseDates(List<String> interrupts) {
        List<Date> interruptsAsDate = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");

        for (String interrupt : interrupts) {
            try {
                interruptsAsDate.add(iso8601DateTimeFormat.parse(interrupt));
            } catch (ParseException e) {
                Log.w("parseDate", "Parsing start time string to date failed in DatabaseHelper.");
            }
        }
        return interruptsAsDate;
    }

    public List<String> getInterrupts() {
        List<String> result = new ArrayList<>();

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + tableNameInterrupts, new String [] {});

        while (cur.moveToNext()) {
            result.add(cur.getString(cur.getColumnIndex(colInterruptTime)));
        }

        return result;
    }

    public void dropTables()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + tableNameLoggedActivities);
        db.execSQL("DROP TABLE IF EXISTS " + tableNameInterrupts);
        onCreate(db);
        db.close();
    }
}
