package se.christofferholmstedt.braintimevsbodytime.core.usecases.viewactivities;

import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.ViewActivitiesBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseModel;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.common.DataProvider;

public class ViewActivities implements ViewActivitiesBoundary {

    private final DataProvider dataProvider;
    private final ActivitiesResponseBoundary activitiesResponseBoundary;

    public ViewActivities (ActivitiesResponseBoundary activitiesResponseBoundary, DataProvider dataProvider)
    {
        this.dataProvider = dataProvider;
        this.activitiesResponseBoundary = activitiesResponseBoundary;
    }

    @Override
    public void getAllActivities() {
        activitiesResponseBoundary.updatePresentationLayer(
                new ActivitiesResponseModel(this.dataProvider.readAll()));
    }
}
