package se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests;

import se.christofferholmstedt.braintimevsbodytime.core.usecases.addtag.TagAlreadyAddedException;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.stopactivity.ActivityNotStartedException;

public interface AddTagBoundary {
    void addTag(Integer id, String tag) throws ActivityNotStartedException, TagAlreadyAddedException;
}
