package se.christofferholmstedt.braintimevsbodytime.core.usecases.startactivity;

import java.time.ZonedDateTime;

import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.StartActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseModel;
import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.common.DataProvider;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.helpers.ActivityStatus;

public class StartActivity implements StartActivityBoundary {

    private final ActivitiesResponseBoundary activitiesResponseBoundary;
    private final DataProvider dataProvider;

    public StartActivity (ActivitiesResponseBoundary activitiesResponseBoundary,
                          DataProvider dataProvider)
    {
        this.activitiesResponseBoundary = activitiesResponseBoundary;
        this.dataProvider = dataProvider;
    }

    @Override
    public void start() throws ActivityAlreadyStartedException
    {
        if (ActivityStatus.isAnyActivityActive(this.dataProvider.readAll()))
        {
            throw new ActivityAlreadyStartedException();
        } else {
            this.dataProvider.add(new Activity(ZonedDateTime.now()));
            this.activitiesResponseBoundary.updatePresentationLayer(
                    new ActivitiesResponseModel(this.dataProvider.readAll())
            );
        }
    }
}
