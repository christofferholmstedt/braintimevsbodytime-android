package se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests;

import se.christofferholmstedt.braintimevsbodytime.core.usecases.stopactivity.ActivityNotStartedException;

public interface StopActivityBoundary {
    void stopActivity(Integer id) throws ActivityNotStartedException;
}
