package se.christofferholmstedt.braintimevsbodytime.core.usecases.common;

import java.util.List;
import java.util.Optional;

import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;

/***
 * "CRUD" interface used by the Core use cases to fetch data
 * from a data provider such as serialized data on disk, SQLite
 * or similar back-end.
 */
public interface DataProvider {

    /***
     * Adds an Activity to the back end.
     *
     * @param activity
     * @return boolean if adding the activity to the back end was successful (true) or failed (false).
     */
    boolean add(Activity activity);

    /***
     * Get one Activity from the data provider.
     *
     * @param id
     * @return Optional.empty() if activity does not exist or Optional.of(Activity) if it exists.
     */
    Optional<Activity> read(Integer id);

    /***
     * Get all Activities from the data provider.
     * @return Empty list if no activities are found otherwise list with all activities found.
     */
    List<Activity> readAll();


    /***
     * Updates/replaces the activity with id supplied in the first parameter with the supplied
     * activity. It is the data providers responsibility to make sure the Activity is stored
     * with the correct id.
     *
     * @param id
     * @param activity
     * @return The updated Optional.of(Activity) with all fields correctly updated if the update went through otherwise Optional.empty().
     */
    Optional<Activity> update(Integer id, Activity activity);

    /***
     * Deletes the Activity with the id supplied.
     *
     * @param id
     * @return boolean that is true if the deletion was successful or false if it failed.
     */
    boolean delete(Integer id);


}
