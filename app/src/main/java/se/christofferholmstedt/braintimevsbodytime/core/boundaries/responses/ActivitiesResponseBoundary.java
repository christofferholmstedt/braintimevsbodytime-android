package se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses;

public interface ActivitiesResponseBoundary {
    void updatePresentationLayer(ActivitiesResponseModel activitiesResponseModel);
}
