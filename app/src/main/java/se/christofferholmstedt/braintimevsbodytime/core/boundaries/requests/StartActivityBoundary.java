package se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests;

import se.christofferholmstedt.braintimevsbodytime.core.usecases.startactivity.ActivityAlreadyStartedException;

public interface StartActivityBoundary {
    void start() throws ActivityAlreadyStartedException;
}
