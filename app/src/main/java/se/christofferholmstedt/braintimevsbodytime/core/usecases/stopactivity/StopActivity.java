package se.christofferholmstedt.braintimevsbodytime.core.usecases.stopactivity;

import android.util.Log;

import java.util.Optional;

import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.StopActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseModel;
import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.common.DataProvider;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.helpers.ActivityFactory;

public class StopActivity implements StopActivityBoundary {

    private final ActivitiesResponseBoundary activitiesResponseBoundary;
    private final DataProvider dataProvider;

    public StopActivity(ActivitiesResponseBoundary activitiesResponseBoundary,
                        DataProvider dataProvider)
    {
        this.activitiesResponseBoundary = activitiesResponseBoundary;
        this.dataProvider = dataProvider;
    }

    @Override
    public void stopActivity(Integer id) throws ActivityNotStartedException
    {

        Log.e("StopActivity", "Stop activity is called");
        Optional<Activity> activity = this.dataProvider.read(id);

        if (activity.isPresent() &&
                activity.get().isActive() &&
                activity.get().getId().isPresent())
        {
            this.dataProvider.update(activity.get().getId().get(),
                                        ActivityFactory.setStopTime(activity.get()));
        }
        else
        {
            Log.e("StopActivity", "Unknown error case, not able to stop activity, what to do?");
        }

        this.activitiesResponseBoundary.updatePresentationLayer(
                new ActivitiesResponseModel(this.dataProvider.readAll())
        );
    }
}
