package se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests;

public interface ViewActivitiesBoundary {
    void getAllActivities();
}
