package se.christofferholmstedt.braintimevsbodytime.core.usecases.interruptactivity;

import android.util.Log;

import java.time.ZonedDateTime;
import java.util.Optional;

import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.InterruptActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseModel;
import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.common.DataProvider;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.helpers.ActivityFactory;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.stopactivity.ActivityNotStartedException;

public class InterruptActivity implements InterruptActivityBoundary {

    private final ActivitiesResponseBoundary activitiesResponseBoundary;
    private final DataProvider dataProvider;

    public InterruptActivity(ActivitiesResponseBoundary activitiesResponseBoundary,
                             DataProvider dataProvider)
    {
        this.activitiesResponseBoundary = activitiesResponseBoundary;
        this.dataProvider = dataProvider;
    }

    @Override
    public void interruptActivity(Integer id) throws ActivityNotStartedException
    {

        Log.e("InterruptActivity", "Interrupt activity is called");
        Optional<Activity> activity = this.dataProvider.read(id);

        if (activity.isPresent() &&
                activity.get().isActive() &&
                activity.get().getId().isPresent())
        {
            activity.get().getInterrupts().add(ZonedDateTime.now());
            this.dataProvider.update(activity.get().getId().get(), activity.get());
        }
        else
        {
            Log.e("InterruptActivity", "Unknown error case, not able to interrupt activity, what to do?");
        }

        this.activitiesResponseBoundary.updatePresentationLayer(
                new ActivitiesResponseModel(this.dataProvider.readAll())
        );
    }
}
