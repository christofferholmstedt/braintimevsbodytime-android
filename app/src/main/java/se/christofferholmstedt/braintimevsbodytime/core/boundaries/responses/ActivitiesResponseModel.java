package se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses;

import java.util.ArrayList;
import java.util.List;

import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;

public class ActivitiesResponseModel {

    private List<Activity> activities = new ArrayList<>();

    public ActivitiesResponseModel (List<Activity> activities)
    {
        this.activities.addAll(activities);
    }

    public List<Activity> getActivities() {
        return activities;
    }
}
