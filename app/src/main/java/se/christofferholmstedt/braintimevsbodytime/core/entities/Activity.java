package se.christofferholmstedt.braintimevsbodytime.core.entities;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Optional;

public class Activity {
    Optional<Integer> id;
    ZonedDateTime startTime;
    Optional<ZonedDateTime> stopTime;
    ArrayList<ZonedDateTime> interrupts;
    ArrayList<String> tags;

    public Activity (ZonedDateTime startTime)
    {
        this.id = Optional.empty();
        this.startTime = startTime;
        this.stopTime = Optional.empty();
        this.interrupts = new ArrayList<>();
        this.tags = new ArrayList<>();
    }

    public Activity (ZonedDateTime startTime,
              ZonedDateTime stopTime,
              ArrayList<ZonedDateTime> interrupts,
              ArrayList<String> tags)
    {
        this.id = Optional.empty();
        this.startTime = startTime;
        this.stopTime = Optional.of(stopTime);
        this.interrupts = interrupts;
        this.tags = tags;
    }

    public Activity (Integer id,
                     ZonedDateTime startTime,
                     ArrayList<ZonedDateTime> interrupts,
                     ArrayList<String> tags)
    {
        this.id = Optional.of(id);
        this.startTime = startTime;
        this.stopTime = Optional.empty();
        this.interrupts = interrupts;
        this.tags = tags;
    }

    public Activity (Integer id,
                     ZonedDateTime startTime,
                     ZonedDateTime stopTime,
                     ArrayList<ZonedDateTime> interrupts,
                     ArrayList<String> tags)
    {
        this.id = Optional.of(id);
        this.startTime = startTime;
        this.stopTime = Optional.of(stopTime);
        this.interrupts = interrupts;
        this.tags = tags;
    }

    public Activity (Optional<Integer> id,
                     ZonedDateTime startTime,
                     ZonedDateTime stopTime,
                     ArrayList<ZonedDateTime> interrupts,
                     ArrayList<String> tags)
    {
        this.id = id;
        this.startTime = startTime;
        this.stopTime = Optional.of(stopTime);
        this.interrupts = interrupts;
        this.tags = tags;
    }

    public Optional<Integer> getId() {
        return id;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public Optional<ZonedDateTime> getStopTime() {
        return stopTime;
    }

    public ArrayList<ZonedDateTime> getInterrupts() {
        return interrupts;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public boolean isActive() {
        if (stopTime.isPresent())
        {
            return false;
        }

        return true;
    }
}
