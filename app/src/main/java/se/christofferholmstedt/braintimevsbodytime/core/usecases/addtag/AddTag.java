package se.christofferholmstedt.braintimevsbodytime.core.usecases.addtag;

import android.util.Log;

import java.time.ZonedDateTime;
import java.util.Optional;

import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.AddTagBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.requests.InterruptActivityBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseBoundary;
import se.christofferholmstedt.braintimevsbodytime.core.boundaries.responses.ActivitiesResponseModel;
import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.common.DataProvider;
import se.christofferholmstedt.braintimevsbodytime.core.usecases.stopactivity.ActivityNotStartedException;

public class AddTag implements AddTagBoundary {

    private final ActivitiesResponseBoundary activitiesResponseBoundary;
    private final DataProvider dataProvider;

    public AddTag(ActivitiesResponseBoundary activitiesResponseBoundary,
                  DataProvider dataProvider)
    {
        this.activitiesResponseBoundary = activitiesResponseBoundary;
        this.dataProvider = dataProvider;
    }

    @Override
    public void addTag(Integer id, String tag) throws ActivityNotStartedException
    {

        Log.e("AddTag", "AddTag is called");
        Optional<Activity> activity = this.dataProvider.read(id);

        if (activity.isPresent() &&
                activity.get().isActive() &&
                activity.get().getId().isPresent())
        {
            if (activity.get().getTags().contains(tag)) {
                throw new TagAlreadyAddedException();
            } else {
                activity.get().getTags().add(tag);
                this.dataProvider.update(activity.get().getId().get(), activity.get());
            }
        }
        else
        {
            Log.e("AddTag", "Unknown error case, not able to add tag to activity, what to do?");
        }

        this.activitiesResponseBoundary.updatePresentationLayer(
                new ActivitiesResponseModel(this.dataProvider.readAll())
        );
    }
}
