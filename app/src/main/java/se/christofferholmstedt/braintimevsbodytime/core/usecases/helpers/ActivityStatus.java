package se.christofferholmstedt.braintimevsbodytime.core.usecases.helpers;

import java.util.List;
import java.util.function.Predicate;

import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;

public class ActivityStatus {

    private ActivityStatus () {}

    public static boolean isAnyActivityActive(List<Activity> activities)
    {
        long matches = activities.stream().filter(Activity::isActive).count();

        if (matches > 0)
            return true;
        else
            return false;
    }
}
