package se.christofferholmstedt.braintimevsbodytime.core.usecases.helpers;

import java.time.ZonedDateTime;
import java.util.Optional;

import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;

public class ActivityFactory {

    private ActivityFactory () {};

    public static Activity setStopTime(Activity activity)
    {
        return new Activity(Optional.of(activity.getId().get()), // Is a new Optional.of(Integer) equal to the first one? Same object ref?
                                        activity.getStartTime(),
                                        ZonedDateTime.now(),
                                        activity.getInterrupts(),
                                        activity.getTags());
    }
}
