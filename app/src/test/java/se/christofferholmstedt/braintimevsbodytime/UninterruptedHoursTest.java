package se.christofferholmstedt.braintimevsbodytime;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class UninterruptedHoursTest {

    @Test
    public void stopTimeBeforeStartTime() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T20:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void sameTime() throws Exception {
        Date start = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, start, interrupts));
    }

    @Test
    public void zeroUninterruptedHours() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T21:59:59.999+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void oneUninterruptedHour() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T22:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T23:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing.
        }
        assertEquals(1, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void twoUninterruptedHours() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T23:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(2, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void twentyFiveUninterruptedHours() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-14T22:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(25, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void oneInterruptedHour() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T22:00:00.000+0100");
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T21:30:00.000+0100"));
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void oneInterruptedHourAndOneUninterruptedHour() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T23:00:00.000+0100");
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T21:30:00.000+0100"));
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(1, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void twoInterruptedHours() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T21:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T23:00:00.000+0100");
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T21:30:00.000+0100"));
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T22:40:00.000+0100"));
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void twoInterruptedHoursAndOneUninterruptedHour() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T19:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T22:00:00.000+0100");
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T19:09:00.000+0100"));
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T21:30:00.000+0100"));
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(2, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void interruptsOutsideOfActivity() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T19:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T22:00:00.000+0100");
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T18:09:00.000+0100"));
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T23:00:00.000+0100"));
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(3, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }

    @Test
    public void stopIsNull() throws Exception {
        Date start = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T19:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, null, interrupts));
    }

    @Test
    public void startIsNull() throws Exception {
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            stop = iso8601DateTimeFormat.parse("2018-01-13T19:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(null, stop, interrupts));
    }

    @Test
    public void interruptsAreNull() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T19:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T19:00:00.000+0100");
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, stop, null));
    }

    @Test
    public void twoInterruptsCloseToEachOther() throws Exception {
        Date start = new Date();
        Date stop = new Date();
        List<Date> interrupts = new ArrayList<>();

        DateFormat iso8601DateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZZZZZ");
        try {
            start = iso8601DateTimeFormat.parse("2018-01-13T20:00:00.000+0100");
            stop = iso8601DateTimeFormat.parse("2018-01-13T21:45:00.000+0100");
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T20:20:00.000+0100"));
            interrupts.add(iso8601DateTimeFormat.parse("2018-01-13T20:30:00.000+0100"));
        } catch (ParseException e) {
            // Do nothing
        }

        assertEquals(0, MainActivity.getUninterruptedHours(start, stop, interrupts));
    }


}