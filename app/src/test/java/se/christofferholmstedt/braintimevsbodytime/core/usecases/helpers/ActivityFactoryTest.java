package se.christofferholmstedt.braintimevsbodytime.core.usecases.helpers;

import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.ArrayList;

import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;

import static org.junit.Assert.*;

public class ActivityFactoryTest {

    @Test
    public void setStopTime() {
        Activity startedActivity = new Activity(34, ZonedDateTime.now(), new ArrayList<>(), new ArrayList<>());
        Activity stoppedActivity = ActivityFactory.setStopTime(startedActivity);

        assertEquals(startedActivity.getId().get(), stoppedActivity.getId().get());
    }
}