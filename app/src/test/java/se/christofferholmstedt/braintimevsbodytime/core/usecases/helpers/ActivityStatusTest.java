package se.christofferholmstedt.braintimevsbodytime.core.usecases.helpers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import se.christofferholmstedt.braintimevsbodytime.core.entities.Activity;

import static org.junit.Assert.*;

public class ActivityStatusTest {

    List<Activity> activities;

    @Before
    public void setUp() throws Exception {
        activities = new ArrayList<>();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void isActivityAlreadyStartedOneActivityStarted() {
        activities.add(new Activity(ZonedDateTime.now()));
        assertTrue(ActivityStatus.isAnyActivityActive(activities));
    }

    @Test
    public void isActivityAlreadyStartedZeroActivityStarted() {
        assertFalse(ActivityStatus.isAnyActivityActive(activities));
    }

    @Test
    public void isActivityAlreadyStartedOneActivityStartedAndStopped() {
        activities.add(new Activity(ZonedDateTime.now(), ZonedDateTime.now(), new ArrayList<>(), new ArrayList<>()));
        assertFalse(ActivityStatus.isAnyActivityActive(activities));
    }

    @Test
    public void isActivityAlreadyStartedMultipleActivitiesOneStarted() {
        activities.add(new Activity(ZonedDateTime.now(), ZonedDateTime.now(), new ArrayList<>(), new ArrayList<>()));
        activities.add(new Activity(ZonedDateTime.now(), ZonedDateTime.now(), new ArrayList<>(), new ArrayList<>()));
        activities.add(new Activity(ZonedDateTime.now(), ZonedDateTime.now(), new ArrayList<>(), new ArrayList<>()));
        activities.add(new Activity(ZonedDateTime.now(), ZonedDateTime.now(), new ArrayList<>(), new ArrayList<>()));
        activities.add(new Activity(ZonedDateTime.now(), ZonedDateTime.now(), new ArrayList<>(), new ArrayList<>()));
        activities.add(new Activity(ZonedDateTime.now()));
        assertTrue(ActivityStatus.isAnyActivityActive(activities));
    }
}